package online.cryptopie.tasks;

import com.mongodb.client.model.changestream.ChangeStreamDocument;
import online.cryptopie.models.Transaction;
import online.cryptopie.services.TelegramService;
import org.bson.Document;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.messaging.ChangeStreamRequest;
import org.springframework.data.mongodb.core.messaging.MessageListener;
import org.springframework.data.mongodb.core.messaging.MessageListenerContainer;
import org.springframework.data.mongodb.core.messaging.Subscription;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static online.cryptopie.models.Status.DENIED;
import static online.cryptopie.models.Type.USER_EXCHANGE;
import static online.cryptopie.models.Type.USER_OUT;

@Component
public class TransactionsChangeStream {
    private final MessageListenerContainer messageListenerContainer;
    private final TelegramService telegramService;

    public TransactionsChangeStream(MessageListenerContainer messageListenerContainer, TelegramService telegramService) {
        this.messageListenerContainer = messageListenerContainer;
        this.telegramService = telegramService;
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println(subscribeToDeclined().isActive());
    }

    public Subscription subscribeToDeclined() {
        final var inTransactionFilter = Aggregation.newAggregation(Aggregation.match(Criteria
                .where("status").is(DENIED)
                .and("type").in(USER_OUT, USER_EXCHANGE)));

        MessageListener<ChangeStreamDocument<Document>, Transaction> messageListener = message -> {
            Transaction newTransaction = message.getBody();
            if (newTransaction != null) {
                telegramService.sendMessageForDeclined(newTransaction);
            }
        };

        ChangeStreamRequest<Transaction> request = ChangeStreamRequest.builder(messageListener)
                .collection("transactions")
                .filter(inTransactionFilter)
                .build();

        messageListenerContainer.start();
        return messageListenerContainer.register(request, Transaction.class);
    }
}
