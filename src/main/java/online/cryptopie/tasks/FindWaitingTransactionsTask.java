package online.cryptopie.tasks;

import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.services.TelegramService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class FindWaitingTransactionsTask {
    private final TelegramService telegramService;
    private final ManualRepository manualRepository;

    public FindWaitingTransactionsTask(TelegramService telegramService, ManualRepository manualRepository) {
        this.telegramService = telegramService;
        this.manualRepository = manualRepository;
    }

    @Scheduled(fixedDelay = 60000)
    public void findWaiting() {
        manualRepository.findWaitingTransactions().forEach(transaction -> {
            if (transaction.getStartDate() != null) {
                var dif = (new Date().getTime() - transaction.getStartDate().getTime()) / 60000;
                if (dif >= 1) {
                    telegramService.sendMessageForLongWait(transaction, (int) dif);
                }
            }
        });
    }
}