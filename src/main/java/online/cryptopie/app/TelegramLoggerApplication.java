package online.cryptopie.app;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Chat;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.SendMessage;
import online.cryptopie.models.LogClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@SpringBootApplication
@EnableScheduling
@EnableAsync
@ComponentScan(basePackages = "online.cryptopie")
@EnableConfigurationProperties
public class TelegramLoggerApplication {
    public static final List<LogClient> logClients = new CopyOnWriteArrayList<>();

    public static void main(String[] args) {
        SpringApplication.run(TelegramLoggerApplication.class, args);
    }

    @Bean
    public TelegramBot getTelegramBot() {
        TelegramBot bot = new TelegramBot("1076176882:AAGTa7fvbVGqS4eLQLni89tasdT-1biOc40");

        bot.setUpdatesListener(updates -> {
            for (Update update : updates) {
                final var text = update.message().text();
                final var chat = update.message().chat();
                final var client = createClient(chat);

                if (text != null) {
                    var mes = "";
                    switch (text) {
                        case "/start":
                            if (logClients.stream().noneMatch(logClient -> logClient.getChatId().equals(client.getChatId()))) {
                                logClients.add(client);
                                mes = String.format("Новый клиент успешно добавлен!\n Активные клиенты в системе:\n%s"
                                        , Arrays.toString(logClients.toArray()));
                            } else {
                                mes = String.format("Клиент уже подписан!\n Активные клиенты в системе:\n%s"
                                        , Arrays.toString(logClients.toArray()));
                            }
                            break;
                        case "/stop":
                            logClients.remove(client);
                            mes = String.format("Клиент успешно удален!\n Активные клиенты в системе:\n%s"
                                    , Arrays.toString(logClients.toArray()));
                            break;
                    }
                    bot.execute(new SendMessage(client.getChatId(), mes).parseMode(ParseMode.Markdown));
                }
            }

            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        });

        return bot;
    }

    private LogClient createClient(Chat chat) {
        final var type = chat.type();

        var client = new LogClient();
        client.setChatId(chat.id());

        if (type.equals(Chat.Type.group)) {
            client.setName(chat.title());
        }
        if (type.equals(Chat.Type.Private)) {
            client.setName(chat.username());
        }

        return client;
    }

    @Bean(name = "threadPoolExecutor")
    @Primary
    public AsyncTaskExecutor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(55);
        executor.setThreadNamePrefix("Async-");

        return executor;
    }
}
