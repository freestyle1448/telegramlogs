package online.cryptopie.app;

import lombok.SneakyThrows;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.core.messaging.DefaultMessageListenerContainer;
import org.springframework.data.mongodb.core.messaging.MessageListenerContainer;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

@Configuration
@EnableMongoRepositories(basePackages = "online.cryptopie.repositories")
@ConfigurationProperties("mongo")
public class MongoConf {
    private final MongoMappingContext mongoMappingContext;
    private String credPath;

    public MongoConf(MongoMappingContext mongoMappingContext) {
        this.mongoMappingContext = mongoMappingContext;
    }

    @SneakyThrows
    @Bean
    public MongoDbFactory mongoDbFactory() {
        final var collect = Files.lines(Paths.get(credPath), StandardCharsets.UTF_8).collect(Collectors.joining());
        return new SimpleMongoClientDbFactory(collect);
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        final var mongoDbFactory = mongoDbFactory();

        var dbRefResolver = new DefaultDbRefResolver(mongoDbFactory);
        final var converter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));

        return new MongoTemplate(mongoDbFactory, converter);
    }

    @Bean
    MessageListenerContainer messageListenerContainer(MongoTemplate template, AsyncTaskExecutor taskExecutor) {
        final var defaultMessageListenerContainer = new DefaultMessageListenerContainer(template, taskExecutor);
        defaultMessageListenerContainer.start();
        return defaultMessageListenerContainer;
    }

    public String getCredPath() {
        return credPath;
    }

    public void setCredPath(String credPath) {
        this.credPath = credPath;
    }
}
