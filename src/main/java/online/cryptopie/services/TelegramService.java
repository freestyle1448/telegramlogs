package online.cryptopie.services;

import online.cryptopie.models.Transaction;

public interface TelegramService {
    void sendMessageForDeclined(Transaction transaction);

    void sendMessageForLongWait(Transaction transactions, int minutesWait);
}
