package online.cryptopie.services;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.SendMessage;
import online.cryptopie.app.TelegramLoggerApplication;
import online.cryptopie.models.QiwiRunnerError;
import online.cryptopie.models.Transaction;
import online.cryptopie.repositories.QiwiErrorsRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import static online.cryptopie.models.RequestTypes.CHECK;
import static online.cryptopie.models.RequestTypes.WITHDRAW;

@Service
@Async
public class TelegramServiceImpl implements TelegramService {
    private final TelegramBot telegramBot;
    private final QiwiErrorsRepository qiwiErrorsRepository;

    public TelegramServiceImpl(TelegramBot telegramBot, QiwiErrorsRepository qiwiErrorsRepository) {
        this.telegramBot = telegramBot;
        this.qiwiErrorsRepository = qiwiErrorsRepository;
    }

    @Override
    public void sendMessageForDeclined(Transaction transaction) {
        TelegramLoggerApplication.logClients.forEach(logClient -> {
            final var optionalLogs = qiwiErrorsRepository
                    .findAllByTransactionNumberAndRequestTypeOrderByDate(String.valueOf(transaction.getTransactionNumber()), WITHDRAW);
            final var optionalCheckLogs = qiwiErrorsRepository
                    .findAllByTransactionNumberAndRequestTypeOrderByDate(String.valueOf(transaction.getTransactionNumber()), CHECK);

            String mes;
            QiwiRunnerError qiwiRunnerError = null;

            if (!optionalCheckLogs.isEmpty()) {
                qiwiRunnerError = optionalCheckLogs.get(optionalCheckLogs.size() - 1);
            } else if (!optionalLogs.isEmpty()) {
                qiwiRunnerError = optionalLogs.get(0);
            }

            mes = getMes(transaction, qiwiRunnerError);

            telegramBot.execute(new SendMessage(logClient.getChatId(), mes).parseMode(ParseMode.Markdown));
        });
    }

    private String getMes(Transaction transaction, QiwiRunnerError qiwiRunnerError) {
        String mes;
        if (qiwiRunnerError != null) {
            if (qiwiRunnerError.getResponse() != null) {
                mes = String.format("*%d* отмена\n" +
                        "запрос - \n" +
                        "`%s`\n" +
                        "ответ - \n" +
                        "`%s`", transaction.getTransactionNumber(), qiwiRunnerError.getRequest(), qiwiRunnerError.getResponse());
            } else {
                mes = String.format("*%d* отмена\n" +
                        "запрос - \n" +
                        "`%s`\n" +
                        "ошибка - \n" +
                        "`%s`", transaction.getTransactionNumber(), qiwiRunnerError.getRequest(), qiwiRunnerError.getFullException());
            }
        } else {
            mes = String.format("*%d* отмена\n" +
                    "не удалось получить запрос и ответ", transaction.getTransactionNumber());
        }

        return mes;
    }

    @Override
    public void sendMessageForLongWait(Transaction transaction, int minutesWait) {
        TelegramLoggerApplication.logClients.forEach(logClient -> {
            var mes = "";
            if (transaction.getTransactionNumber() != null) {
                mes = String.format("*%d* со статусом %d в ожидании больше %d минут",
                        transaction.getTransactionNumber(), transaction.getStatus(), minutesWait);
            } else {
                mes = String.format("Перевод от *%s* к *%s* со статусом %d в ожидании больше %d минут",
                        transaction.getSenderCredentials().getAccount(), transaction.getReceiverCredentials().getAccount(), transaction.getStatus(), minutesWait);
            }
            telegramBot.execute(new SendMessage(logClient.getChatId(), mes).parseMode(ParseMode.Markdown));
        });
    }
}
