package online.cryptopie.models;

public final class Status {
    public static final Integer DENIED = -1;
    public static final Integer WAITING = 0;
    public static final Integer IN_PROCESS = 1;
    public static final Integer SUCCESS = 2;
    public static final Integer MANUAL = 5;
}
