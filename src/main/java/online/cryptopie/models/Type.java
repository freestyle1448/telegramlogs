package online.cryptopie.models;

public final class Type {
    public static final String INCOME = "in";
    public static final String OUTCOME = "out";
    public static final String USER_OUT = "usrOut";
    public static final String USER_EXCHANGE = "usrExc";
    public static final String ADMIN_EXCHANGE = "admExc";
    public static final String ADMIN_FUNDING = "funding";
    public static final String COMMISSION = "commission";
    public static final String PAY_EXCHANGE = "pay";
}