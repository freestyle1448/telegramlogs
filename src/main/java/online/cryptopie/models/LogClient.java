package online.cryptopie.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LogClient {
    private String name;
    private Long chatId;

    @Override
    public String toString() {
        return "client" +
                "name='" + name + '\'' +
                ", chatId=" + chatId;
    }
}
