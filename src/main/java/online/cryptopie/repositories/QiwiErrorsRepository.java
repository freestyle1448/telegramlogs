package online.cryptopie.repositories;

import online.cryptopie.models.QiwiRunnerError;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface QiwiErrorsRepository extends MongoRepository<QiwiRunnerError, ObjectId> {
    List<QiwiRunnerError> findAllByTransactionNumberAndRequestTypeOrderByDate(String transactionNumber, String requestType);
}
