package online.cryptopie.repositories;

import online.cryptopie.models.Transaction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import static online.cryptopie.models.Status.IN_PROCESS;
import static online.cryptopie.models.Status.WAITING;
import static online.cryptopie.models.Type.USER_EXCHANGE;
import static online.cryptopie.models.Type.USER_OUT;

@Repository
public class ManualRepository {
    private final MongoTemplate mongoTemplate;

    public ManualRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public List<Transaction> findWaitingTransactions() {
        var findTransactions = new Query(Criteria
                .where("type").in(USER_OUT, USER_EXCHANGE)
                .and("status").in(WAITING, IN_PROCESS));

        return mongoTemplate.find(findTransactions, Transaction.class);
    }
}
